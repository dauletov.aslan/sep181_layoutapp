package kz.astana.layoutapp;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout ll = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                );
        ll.setLayoutParams(layoutParams);
        ll.setBackgroundResource(R.color.my);

        TextView tv = new TextView(this);
        tv.setText("My new text view");
        tv.setTextSize(50f);

        ll.addView(tv);

        setContentView(ll);
    }
}