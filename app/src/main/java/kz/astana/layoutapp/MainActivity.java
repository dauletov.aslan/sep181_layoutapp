package kz.astana.layoutapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FrameLayout fl;
    private LinearLayout ll;
    private RelativeLayout rl;
    private GridLayout gl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fl = findViewById(R.id.frameLayout);
        ll = findViewById(R.id.linearLayout);
        rl = findViewById(R.id.relativeLayout);
        gl = findViewById(R.id.gridLayout);

        Button one = findViewById(R.id.one);
        Button two = findViewById(R.id.two);
        Button three = findViewById(R.id.three);
        Button four = findViewById(R.id.four);
        Button five = findViewById(R.id.five);

        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.one:
                fl.setVisibility(View.VISIBLE);
                ll.setVisibility(View.GONE);
                break;
            case R.id.two:
                fl.setVisibility(View.GONE);
                ll.setVisibility(View.VISIBLE);
                gl.setVisibility(View.VISIBLE);
                rl.setVisibility(View.VISIBLE);
                break;
            case R.id.three:
                rl.setVisibility(View.INVISIBLE);
                gl.setVisibility(View.VISIBLE);
                break;
            case R.id.four:
                gl.setVisibility(View.INVISIBLE);
                rl.setVisibility(View.VISIBLE);
                break;
            case R.id.five:
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);
        }
    }
}